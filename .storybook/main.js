module.exports = {
  stories: ['../projects/logger-library/**/*.stories.mdx', '../projects/logger-library/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials', '@storybook/addon-notes/register'],
  core: {
    builder: 'webpack5',
  },
};
