const config = require('@fathom/project-config');

module.exports = {
  ...config.jest,
  setupFilesAfterEnv: ['<rootDir>/jest.base.setup.ts'],
};
