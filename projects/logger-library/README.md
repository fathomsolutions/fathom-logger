# @fathom/logger

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.0.

## Code scaffolding

Run `ng generate component component-name --project logger-library` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project logger-library`.

> Note: Don't forget to add `--project logger-library` or else it will be added to the default project in your `angular.json` file.

## Build

Run `ng build logger-library` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build logger-library`, go to the dist folder `cd dist/logger-library` and run `npm publish`.

## Running unit tests

Run `ng test logger-library` to execute the unit tests via [Jest](https://jestjs.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
