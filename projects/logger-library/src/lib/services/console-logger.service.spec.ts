/* eslint-disable @typescript-eslint/no-confusing-void-expression */
import { TestBed } from '@angular/core/testing';

import { ConsoleLoggerService } from './console-logger.service';

describe('ConsoleLoggerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConsoleLoggerService = TestBed.inject(ConsoleLoggerService);
    expect(service).toBeTruthy();
  });

  it('to invoke the log console', () => {
    const service: ConsoleLoggerService = TestBed.inject(ConsoleLoggerService);
    const type = 'test';
    const consoleSpy = jest.spyOn(console, 'log');
    service.invokeConsoleMethod(type);
    expect(consoleSpy).toHaveBeenCalled();
  });
  it('to invoke the info console', () => {
    const service: ConsoleLoggerService = TestBed.inject(ConsoleLoggerService);
    const type = 'info';
    const consoleSpy = jest.spyOn(console, 'info');
    service.invokeConsoleMethod(type);
    expect(consoleSpy).toHaveBeenCalled();
  });
  it('to invoke the warn console', () => {
    const service: ConsoleLoggerService = TestBed.inject(ConsoleLoggerService);
    const type = 'warn';
    const consoleSpy = jest.spyOn(console, 'warn');
    service.invokeConsoleMethod(type);
    expect(consoleSpy).toHaveBeenCalled();
  });
  it('to invoke the error console', () => {
    const service: ConsoleLoggerService = TestBed.inject(ConsoleLoggerService);
    const type = 'error';
    const consoleSpy = jest.spyOn(console, 'error');
    service.invokeConsoleMethod(type);
    expect(consoleSpy).toHaveBeenCalled();
  });
  it('to invoke the debug console', () => {
    const service: ConsoleLoggerService = TestBed.inject(ConsoleLoggerService);
    const type = 'debug';
    const consoleSpy = jest.spyOn(console, 'debug');
    service.invokeConsoleMethod(type);
    expect(consoleSpy).toHaveBeenCalled();
  });
});
