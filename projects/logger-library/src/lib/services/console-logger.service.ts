import { Injectable, Inject } from '@angular/core';
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { Logger } from './logger.service';
import { IS_DEBUG_MODE, IS_DEEP_DEBUGGING_ENABLED } from './mode-tokens';

const noop = (): unknown => undefined;

@Injectable({
  providedIn: 'root',
})
export class ConsoleLoggerService implements Logger {
  constructor(
    @Inject(IS_DEBUG_MODE) private isDebugMode: boolean,
    @Inject(IS_DEEP_DEBUGGING_ENABLED) private isDeepDebuggingEnabled: boolean,
  ) {}

  get info(): unknown {
    if (this.isDebugMode) {
      return console.info.bind(console);
    } else {
      return noop;
    }
  }

  get log(): unknown {
    if (this.isDebugMode) {
      return console.log.bind(console);
    } else {
      return noop;
    }
  }

  get warn(): unknown {
    if (this.isDebugMode) {
      return console.warn.bind(console);
    } else {
      return noop;
    }
  }

  get error(): unknown {
    return console.error.bind(console);
  }

  get debug(): unknown {
    if (this.isDeepDebuggingEnabled) {
      return console.debug.bind(console);
    } else {
      return noop;
    }
  }

  /**This method is to invoke the console.
   * @param {string} type this parameter used to specify the console type.
   * @param {unknown} args this is an optional parameter to specify the printed message in the console.
   */
  invokeConsoleMethod(type: string, args?: unknown): void {
    switch (type) {
      case 'warn': {
        if (args != null) {
          console.warn(args);
        } else {
          console.warn();
        }
        break;
      }
      case 'info': {
        if (args != null) {
          console.info(args);
        } else {
          console.info();
        }
        break;
      }
      case 'error': {
        if (args != null) {
          console.error(args);
        } else {
          console.error();
        }
        break;
      }
      case 'debug': {
        if (args != null) {
          console.debug(args);
        } else {
          console.debug();
        }
        break;
      }
      default: {
        if (args != null) {
          console.log(args);
        } else {
          console.log();
        }
      }
    }
  }
}
