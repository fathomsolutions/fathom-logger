﻿import { Injectable } from '@angular/core';

export abstract class Logger {
  info: unknown;
  warn: unknown;
  error: unknown;
  debug: unknown;
}

@Injectable({
  providedIn: 'root',
})
export class LoggerService implements Logger {
  log: unknown;
  info: unknown;
  warn: unknown;
  error: unknown;
  debug: unknown;
  /**This method is an abstract method to invoke the console.
   * @param {string}_type this parameter used to specify the console type.
   * @param {unknown}_args this is an optional parameter to specify the printed message in the console.
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  invokeConsoleMethod(_type: string, _args?: unknown): void {}
}
