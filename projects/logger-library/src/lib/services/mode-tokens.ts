import { InjectionToken } from '@angular/core';

export const IS_DEBUG_MODE = new InjectionToken<boolean>('IS_DEBUG_MODE', {
  providedIn: 'root',
  factory: (): boolean => true,
});
export const IS_DEEP_DEBUGGING_ENABLED = new InjectionToken<boolean>('IS_DEEP_DEBUGGING_ENABLEDE', {
  providedIn: 'root',
  factory: (): boolean => false,
});
