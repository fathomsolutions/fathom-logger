import { Component } from '@angular/core';

import { LoggerService } from '../../../logger-library/src/lib/services/logger.service';

@Component({
  selector: 'fs-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  constructor(logger: LoggerService) {
    logger.invokeConsoleMethod('info', 'AppComponent: logger.invokeConsoleMethod()');
    logger.invokeConsoleMethod('warn', 'AppComponent: logger.invokeConsoleMethod()');
    logger.invokeConsoleMethod('error', 'AppComponent: logger.invokeConsoleMethod()');
  }
}
