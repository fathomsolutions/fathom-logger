import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ConsoleLoggerService, LoggerService } from '@fathom/logger';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [],
  imports: [BrowserModule],
  providers: [{ provide: LoggerService, useClass: ConsoleLoggerService }],
  bootstrap: [AppComponent],
})
export class AppModule {}
